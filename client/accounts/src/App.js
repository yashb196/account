import React, { Component } from "react";
import ToggleBox from "./components/ToggleBox";
import ToggleBox1 from "./components/ToggleBox1"
import Vehicles from "./components/Vehicles";
import Example2 from "./components/example2"
class App extends Component {
	render(){
		return (
      <>
			<ToggleBox title="Show Vehicles">
				<Vehicles />
			</ToggleBox>
      
      <ToggleBox1 title="show examples">
        <Example2/>
      </ToggleBox1>
      </>
		);
	}
}

export default App;