import React from "react";

class ToggleBox1 extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
            clicked:false
		};
		this.toggleBox = this.toggleBox.bind(this);
	}
  
	toggleBox() {
		const { clicked } = this.state;
		this.setState({
            clicked:!clicked
		});
	}
  
	render() {
		var { title,children } = this.props;
		const { clicked } = this.state;

        if(clicked){
            title='hide example'
        }else{
            title='show example'
        }

		return (
            <div className="example">
                <div className="box1">
				<div className="boxTitle1" onClick={this.toggleBox}>
					{title}
				</div>
                {clicked &&(<div className="boxname">{children}</div>)}
			</div>
            </div>
		);
	}
}

export default ToggleBox1;